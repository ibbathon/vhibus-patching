import os
from datetime import date


class NormalExample:
    def some_func(self):
        return date.today()


class DictExample:
    def some_func(self):
        return os.getenv('HOME', 'oops'), os.getenv('UNKNOWN_ENVVAR')


class ObjExample:
    def some_func(self):
        raise Exception("Shouldn't happen")
