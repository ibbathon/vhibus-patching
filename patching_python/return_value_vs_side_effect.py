class ReturnValueVsSideEffect:
    def some_func(self):
        return 0

    def other_func(self, inpt):
        return inpt
