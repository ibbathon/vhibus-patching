import pytest
from return_value_vs_side_effect import ReturnValueVsSideEffect


class TestReturnValueVsSideEffect:
    def test_return_value(self, mocker):
        rvvse = ReturnValueVsSideEffect()
        mocker.patch.object(rvvse, 'some_func', return_value=7)
        rv = rvvse.some_func()
        rvvse.some_func.assert_called_once_with()
        assert rv == 7

    def test_return_value_multivalue(self, mocker):
        rvvse = ReturnValueVsSideEffect()
        mocker.patch.object(rvvse, 'some_func', return_value=(1, 2))
        rv1 = rvvse.some_func()
        rv2 = rvvse.some_func()
        assert rvvse.some_func.call_count == 2
        assert rv1 == (1, 2)
        assert rv2 == (1, 2)

    def test_side_effect_multivalue(self, mocker):
        rvvse = ReturnValueVsSideEffect()
        mocker.patch.object(rvvse, 'some_func', side_effect=(1, 2))
        rv1 = rvvse.some_func()
        rv2 = rvvse.some_func()
        assert rvvse.some_func.call_count == 2
        assert rv1 == 1
        assert rv2 == 2

    def test_side_effect_exception(self, mocker):
        rvvse = ReturnValueVsSideEffect()
        mocker.patch.object(rvvse, 'some_func', side_effect=Exception('test'))
        with pytest.raises(Exception) as exc_info:
            rvvse.some_func()
        assert str(exc_info.value) == 'test'

    def test_wraps(self, mocker):
        rvvse = ReturnValueVsSideEffect()
        mocker.patch.object(rvvse, 'other_func', wraps=rvvse.other_func)
        rv1 = rvvse.other_func(1)
        rv2 = rvvse.other_func(2)
        assert rvvse.other_func.call_count == 2
        assert rvvse.other_func.call_args_list == [
            mocker.call(1),
            mocker.call(2),
        ]
        assert rv1 == 1
        assert rv2 == 2
